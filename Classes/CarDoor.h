//
//  CarDoor.h
//  Classes
//
//  Created by Alexander Dupree on 01/04/2017.
//  Copyright © 2017 [~/psycho/path>_]. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;


@interface CarDoor : NSObject

@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) NSString *material;

- (void)open;
- (void)close;

@end
