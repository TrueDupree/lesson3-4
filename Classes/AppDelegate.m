//
//  AppDelegate.m
//  Classes
//
//  Created by Alexander Dupree on 01/04/2017.
//  Copyright © 2017 [~/psycho/path>_]. All rights reserved.
//

#import "AppDelegate.h"
#import "DeLorean.h"
#import "CarDoor.h"
#import "CarWheel.h"
#import "Engine.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self testDrive];
    
    return YES;
}


- (void)testDrive {
    DeLorean *machine = [self assembleDeLorean];
    
    [machine openBonnet];
    if (machine.engine) {
        [machine closeBonnet];
        [machine openDoor:machine.doors.firstObject];
        [machine closeDoor:machine.doors.firstObject];
        [machine startEngine];
        [machine drive];
        [machine travelToDate:[NSDate dateWithTimeIntervalSince1970:0]];
        if ([machine conformsToProtocol:@protocol(TimeMachine)]) {
            NSLog(@"Conforms!");
        }
        if ([machine respondsToSelector:@selector(bringMeBackToNowadays)]) {
            NSLog(@"machine responds to selector bringMeBackToNowadays");
            [machine bringMeBackToNowadays];
        }
        
    }
}


#pragma mark - Car Assembly

- (DeLorean *)assembleDeLorean {
    DeLorean *deLorean = [DeLorean new];
    
    deLorean.doors = @[
                       [self door],
                       [self door]
                       ];
    
    deLorean.wheels = @[
                        [self wheel],
                        [self wheel],
                        [self wheel],
                        [self wheel]
                        ];
    return deLorean;
}

- (CarDoor *)door {
    return [CarDoor new];
}

- (CarWheel *)wheel {
    return [CarWheel new];
}



@end
