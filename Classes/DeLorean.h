//
//  DeLorean.h
//  Classes
//
//  Created by Alexander Dupree on 01/04/2017.
//  Copyright © 2017 [~/psycho/path>_]. All rights reserved.
//

@import Foundation;

@class CarWheel, Engine, CarDoor;

#import "TimeMachine.h"

@interface DeLorean : NSObject <TimeMachine>

@property (nonatomic, strong, readonly) Engine *engine;
@property (nonatomic, copy) NSArray <CarWheel*> *wheels;
@property (nonatomic, copy) NSArray <CarDoor*> *doors;
@property (getter=isCrashed) BOOL crashed;
@property (getter=isPainted) BOOL painted;


/**
 create an instance of DeLorean with 
 specified instance of type `Engine`
 
 @param engine  -   a particular engine for our DeLorean
 
 @return    an instance of our DeLorean
 */
- (instancetype)initWithEngine:(Engine *)engine;
+ (instancetype)deLoreanWithEngine:(Engine *)engine;

- (void)startEngine;
- (void)stopEngine;
- (void)drive;
- (void)openDoor:(CarDoor *)door;
- (void)closeDoor:(CarDoor *)door;
- (void)openBonnet;
- (void)closeBonnet;

@end


