//
//  Engine.h
//  Classes
//
//  Created by Alexander Dupree on 01/04/2017.
//  Copyright © 2017 [~/psycho/path>_]. All rights reserved.
//

@import Foundation;

@interface Engine : NSObject

@property (nonatomic, copy) NSString *type;
@property (nonatomic, assign) NSUInteger numberOfCylinders;
@property (nonatomic, assign) float copacity;
@property (getter=isStarted, readonly) BOOL started;

- (void)start;
- (void)stop;

@end
