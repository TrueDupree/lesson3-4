//
//  CarWheel.h
//  Classes
//
//  Created by Alexander Dupree on 01/04/2017.
//  Copyright © 2017 [~/psycho/path>_]. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarWheel : NSObject {
    @public
    float radius;
    @private
    NSString *diskMaterial;
    @protected
    float pressureLevel;
}


@end
