//
//  Engine.m
//  Classes
//
//  Created by Alexander Dupree on 01/04/2017.
//  Copyright © 2017 [~/psycho/path>_]. All rights reserved.
//

#import "Engine.h"

@implementation Engine

+ (instancetype)engineWithParams:(NSDictionary *)params {
    Engine *engine = [Engine new];
    if (engine) {
        engine->_started = YES;
    }
    return engine;
}

- (void)start {
    _started = YES;
    NSLog(@"[Computer:] Engine was started.");
}

- (void)stop {
    _started = NO;
    NSLog(@"[Computer:] Engine was stopped.");
}

@end
