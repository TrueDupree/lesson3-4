//
//  DeLorean.m
//  Classes
//
//  Created by Alexander Dupree on 01/04/2017.
//  Copyright © 2017 [~/psycho/path>_]. All rights reserved.
//

#import "DeLorean.h"
#import "Engine.h"
#import "CarDoor.h"
#import "CarWheel.h"

@interface DeLorean()

@property (nonatomic, strong) Engine *carEngine;

@end

@implementation DeLorean

#pragma mark - Initialization

- (instancetype)init {
    self = [super init];
    if (self) {
        // TODO: define necessary variables here
        
        // FIXME: do smth
        self.carEngine = [Engine new];
    }
    return self;
}

- (instancetype)initWithEngine:(Engine *)engine {
    self = [super init];
    if (self) {
        self.carEngine = engine;
    }
    return self;
}

+ (instancetype)deLoreanWithEngine:(Engine *)engine {
    return [[self alloc] initWithEngine:engine];
}



#pragma mark - Implementation

- (void)startEngine {
    [self.carEngine start];
}

- (void)stopEngine {
    [self.carEngine stop];
}


- (void)drive {
    if (self.carEngine.isStarted) {
        NSLog(@"Live is a motion!");
    } else {
        NSLog(@"[System error:] The engine ain't started!");
    }
}

- (void)openDoor:(CarDoor *)door {
    if ([self.doors containsObject:door]) {
        [door open];
    }
}

- (void)closeDoor:(CarDoor *)door {
    if ([self.doors containsObject:door]) {
        [door close];
    }
}

- (void)openBonnet {
    self->_engine = self.carEngine;
}

- (void)closeBonnet {
    self->_engine = nil;
}


#pragma mark - TimeMachine Protocol Implementation

- (void)travelToDate:(NSDate *)date {
    NSString *dateString = [self stringFromDate:date];
    NSLog(@"Welcome to %@!", dateString);
}

- (void)bringMeBackToNowadays {
    NSString *dateString = [self stringFromDate:[NSDate date]];
    NSLog(@"Welcome to %@!", dateString);
}

- (NSString *)stringFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

@end
