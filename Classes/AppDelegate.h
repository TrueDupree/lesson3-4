//
//  AppDelegate.h
//  Classes
//
//  Created by Alexander Dupree on 01/04/2017.
//  Copyright © 2017 [~/psycho/path>_]. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

