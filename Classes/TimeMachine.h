//
//  TimeMachine.h
//  Classes
//
//  Created by Alexander Dupree on 02/04/2017.
//  Copyright © 2017 [~/psycho/path>_]. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TimeMachine <NSObject>

@required

- (void)travelToDate:(NSDate *)date;


@optional

- (void)bringMeBackToNowadays;

@end
